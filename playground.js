var prompt = require('prompt');
var colors = require("colors/safe");
prompt.message = colors.rainbow("Question!");
prompt.start();

prompt.get(['param1', 'param2'], function (err, result) {
  if (err) { return onErr(err); }
  console.log('Command-line input received:');
  console.log(colors.cyan('  輸入1: ' + result.param1));
  console.log(colors.cyan('  輸入2: ' + result.param2));

  //==================================
  //把邏輯寫在這個下面
  // 輸入1使用 result.param1
  // 輸入2使用 result.param2
  //




  //==================================
});

function onErr(err) {
  console.log(err);
  return 1;
}